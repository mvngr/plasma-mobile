/*
 * SPDX-FileCopyrightText: 2022 Devin Lin <devin@kde.org>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.15
import QtFeedback 5.0

/**
 * Private component that wraps a QtFeedback HapticsEffect, so that we can optionally load
 * this component.
 */

HapticsEffect {}
